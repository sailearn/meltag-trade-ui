import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  imports: [
    HomeRoutingModule,
    ThemeModule,
    NbMenuModule,
    NgxPermissionsModule.forChild()
  ],
  declarations: [
    HomeComponent,
  ],
})
export class HomeModule {
}
