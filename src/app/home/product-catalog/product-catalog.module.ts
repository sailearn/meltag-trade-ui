import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SkusComponent } from './skus/skus.component';
import { ProductsComponent } from './products/products.component';
import { ProductGroupComponent } from './product-group/product-group.component';
export const routes: Routes = [
  { path: '', component: SkusComponent },
  { path: 'product-group', component: ProductGroupComponent },
  { path: 'products', component: ProductsComponent },
  {path : '**', component : SkusComponent},

];
@NgModule({
  declarations: [SkusComponent, ProductsComponent, ProductGroupComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
})
export class ProductCatalogModule { }
