import { Routes } from '@angular/router';
import { InboundComponent } from './inbound/inbound.component';
import { OutboundComponent } from './outbound/outbound.component';
import { AssignCsmComponent } from './assign-csm/assign-csm.component';
import { CouponStatusComponent } from './coupon-status/coupon-status.component';
import { LookupUserComponent } from './lookup-user/lookup-user.component';
import { NgxPermissionsGuard } from 'ngx-permissions';

export const routes: Routes = [
  {
    path: '',
    component: InboundComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin', 'bpo-agent'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: 'outbound',
    component: OutboundComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin', 'bpo-agent'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: 'assign-csm',
    component: AssignCsmComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: 'coupon-status',
    component: CouponStatusComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin', 'bpo-agent'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: 'lookup-user',
    component: LookupUserComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin', 'bpo-agent'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: '**',
    component: InboundComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin', 'bpo-agent'],
        redirectTo: '/auth',
      },
    },
  },
];
