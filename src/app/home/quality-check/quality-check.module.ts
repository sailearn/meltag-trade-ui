import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routes } from './quality-check-routing.module';
import { RouterModule } from '@angular/router';
import { QualifiedComponent } from './qualified/qualified.component';
import { DisqualifiedComponent } from './disqualified/disqualified.component';
import { ToBeQualifiedComponent } from './to-be-qualified/to-be-qualified.component';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  declarations: [QualifiedComponent, DisqualifiedComponent, ToBeQualifiedComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPermissionsModule.forChild(),
  ],
})
export class QualityCheckModule { }
