import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './home-menu';
import { BPO_ADMIN } from './home-menu';
import { BPO_AGENT } from './home-menu';
import { FINANCE_MENU } from './home-menu';
import { NgxPermissionsService } from 'ngx-permissions';


@Component({
  selector: 'ngx-home',
  styleUrls: ['home.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class HomeComponent implements OnInit {
  private perm: String = 'finance';
  constructor(private permissionsService: NgxPermissionsService) {}
  private permissionsArray = ['finance'];
  menu;
  ngOnInit() {
    this.permissionsService.loadPermissions(this.permissionsArray);
    this.menu = (this.perm === 'admin') ?
    MENU_ITEMS : (this.perm === 'bpo-agent') ?
    BPO_AGENT : (this.perm === 'bpo-admin') ?
    BPO_ADMIN : (this.perm === 'finance') ?
    FINANCE_MENU : MENU_ITEMS;
  }
}
