const express = require('express');
const path = require('path');
const app = express();
const compression = require('compression')
app.use(compression())

app.use(express.static(__dirname + '/dist'))
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '/dist/index.html'))
})

app.listen(process.env.PORT || 3000, () => {
    console.log('Trade Launched!!!!!')
})
